<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Fraternal;
use App\Models\Motorclub;
use App\Models\Prepaid;
use App\Models\Purchase;
use App\Models\Risk;

class SearchController extends Controller
{
    public function index()
    {
        return view('search');
    }

    public function companies()
    {
        $companies = Company::with('lobs')->get();
        return response()->json($companies);
    }

    public function fraternals()
    {
        $fraternals = Fraternal::with('lobs')->get();
        return response()->json($fraternals);
    }

    public function motorclubs()
    {
        $motorclubs = Motorclub::with('lobs')->get();
        return response()->json($motorclubs);
    }

    public function prepaids()
    {
        $prepaids = Prepaid::with('lobs')->get();
        return response()->json($prepaids);
    }

    public function purchases()
    {
        $purchases = Purchase::with('lobs')->get();
        return response()->json($purchases);
    }

    public function risks()
    {
        $risks = Risk::with('lobs')->get();
        return response()->json($risks);
    }
}
