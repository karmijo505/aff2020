<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $primaryKey = 'naic_cocode';

    public function lobs() {

        return $this->belongsToMany('App\Models\Lob','company_lob' , 'naic_cocode', 'lob_id', 'naic_cocode', 'lob_id');
    }

    public function npns() {

        return $this->hasMany('App\Models\Npn', 'naic_cocode', 'naic_cocode');
    }
}
