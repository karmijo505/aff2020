<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Npn extends Model
{
    public function companies() {
        return $this->belongsTo("App\Models\Company");
    }
}
