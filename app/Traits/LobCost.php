<?php

namespace App\Traits;

trait LobCost
{

    // business rule: set single (lob) cost or multi (lob) cost for each lob
    public function setLobCost($lobs)
    {

        //dd($lobs);
        return $lobs[0][0];
    }
}