<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            CompaniesTableSeeder::class,
            CompanyLobTableSeeder::class,
            LobsTableSeeder::class,
            FraternalsTableSeeder::class,
            MotorclubsTableSeeder::class,
            PrepaidsTableSeeder::class,
            PurchasesTableSeeder::class,
            RisksTableSeeder::class,
            NpnTableSeeder::class
        ]);
    }
}
