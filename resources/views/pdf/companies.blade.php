<html>
<head>
    <style>
        .headcenter {
            text-align: center;
            color: red;
        }

        .w-100 {
            width: 100%;
        }

        .w-75 {
            width: 75%;
        }

        .w-65 {
            width: 65%;
        }

        .w-60 {
            width: 60%;
        }

        .w-55 {
            width: 55%;
        }

        .w-50 {
            width: 50%;
        }

        .w-45 {
            width: 45%;
        }

        .w-40 {
            width: 40%;
        }

        .w-35 {
            width: 35%;
        }

        .w-30 {
            width: 30%;
        }

        .w-20 {
            width: 20%;
        }

        .f-left {
            float: left;
        }

        .f-right {
            float: right;
        }

        .t-left {
            text-align: left;
        }

        .t-right {
            text-align: right;
        }

        .t-center {
            text-align: center
        }

        .t-size10 {
            font-size: 1.10em;
        }

        .t-size12 {
            font-size: 12px;
        }

        .t-size16 {
            font-size: 16px;
        }

        .center {
            margin: 0 auto;
        }

        .bold {
            font-weight: bold;
        }

        .red-text {
            color: red;
        }

        .company-info {
            padding: 10px;
        }

        .company-info-2 {
            padding: 10px 0;
        }

        .notification-info {
            padding: 35px 0;
        }

        .office-use-info {
            padding: 25px;
        }

        .foot {
            position: fixed;
            text-align: center;
            width: 100%;
            padding: 30px;
            bottom: 0;
        }

        .mt-30 {
            margin-top: 30px;
        }

        .text-center {
            text-align: center;
        }

        table th {
            border: 1px solid #444;
        }

        fieldset {
            border: 1px solid #444;
            padding: 15px;
            width: 50%;
        }

        @page {
            margin-top: 25px;
            margin-bottom: 25px;
            margin-left: 25px;
            margin-right: 25px;
        }
    </style>
</head>


@extends('layouts.header')

@section('content')

    <div>
        <div class="company-info f-left w-45 t-size12">
            <div>License #: {{ $entityData['naic_cocode'] }}</div>
            <div>{{ $entityData['company_name'] }}</div>
            <div>{{ $entityData['mlg_address1'] }}</div>
            <span>{{ $entityData['mailing_city'] }}</span>
            <span>{{ $entityData['mailing_state'] }}, </span>
            <span>{{ $entityData['mailing_zip'] }}</span>
        </div>

        <div class="f-right w-45 t-left">
            <div class="company-info f-left w-45 t-left t-size12">
                <div>Date Invoice Generated</div>
                <div>Invoice Number</div>
                <div>Year</div>
                <div>Total Amount Due By</div>
            </div>
            <div class="company-info f-right w-30 t-left t-size12">
                <div>{{ $noticeDate }}</div>
                <div>{{ $invoiceNbr }}</div>
                <div>{{ $taxableYear }}</div>
                <div>{{ $dueDate }}</div>
            </div>
        </div>
    </div>
    <div>
        <div class="t-size12 mt-30 red-text">
        <span style="background-color: #ffff00">
            <span class="bold">Important:  Checks are required as form of payment.</span>
            <span class="bold">Payment must be made in full.</span>
        </span>
        </div>
    </div>
    <div>
        <h3 class="t-center">NOTICE OF AFFILIATION RENEWAL FEE</h3>

        <table width="100%">
            <tr>
                <th>Line of Business</th>
                <th>Affiliate Quantity</th>
                <th>Amount</th>
            </tr>
            <tr>
                <td>
                    {{ $entityData['lobs']['line_of_business'] }}
                </td>
                <td class="t-center">
                    {{ $entityData['qty_aff']}}
                </td>
                <td class="t-center">
                    ${{ money_format('%.2n',$entityData['lobs']['cost']) }}
                </td>
            </tr>
            <tr>
                <td class="t-right">Total amount to be Remitted:</td>
                <td class="t-center bold">${{ money_format('%.2n', $totalAmt) }}</td>
            </tr>
        </table>
    </div>

    <div class="notification-info w-100 t-left t-size06">
        These fees are based on data as of February 18, 2020.<br>
        When filling the AFFILIATION RENEWAL FEE, a separate check must be submitted and should not be combined with any
        other fees. Questions regarding the filing fee should be directed to the contact information below.
    </div>
    <div class="payment-info">
        <h3 class="t-center">Please Return Copy of This Notice With Your Payment</h3>
        <h3 class="t-left">Make Check Payable to:</h3>
    </div>

    <div>
        <div class="f-left w-35">
            <div class="t-left t-size10 bold">NM OSI</div>
            <div class="f-left w-100 t-left t-size12">
                <div>Producer Licensing Bureau</div>
                <div>P.O. Box 1689</div>
                <div>Santa Fe, New Mexico 87504-1689</div>
                <div>(505) 827-4349</div>
            </div>
        </div>

        <div class="f-left w-65">
            <div class="w-100 f-left t-left t-size10">
                <fieldset class="t-left w-100">
                    <legend>For OSI Use Only</legend>
                    Check No.
                    <hr/>
                    Amount Remitted $
                    <hr/>
                </fieldset>
            </div>
        </div>
    </div>

    <div class="foot" style="margin-top:10px">
        <img src="/images/osi-transparent.png" width="125px" height="80px">
    </div>
    <div class="headcenter t-size12" style="margin-bottom: -10px">
        <div>Main Office: 1120 Paseo de Peralta, Room 428, Santa Fe, NM 87501</div>
        <div>Satellite Office: 6200 Uptown Blvd NE, Suite 100, Albuquerque, NM 87110</div>
        <div>Main Phone: (505) 827-4601 | Satellite Phone: (505) 322-2186 | Toll Free: (855) 4-ASK-OSI</div>
        <div>www.osi.state.nm.us</div>
    </div>

    <p style="page-break-before: always">
        <br>
    <div>
        <h2 class="headcenter">STATE OF NEW MEXICO<br>
            OFFICE OF SUPERINTENDENT OF INSURANCE</h2>
        <div style="margin-bottom:15px">
            <div class="f-left w-25">
                <div class="headcenter">SUPERINTENDENT</div>
                <div class="headcenter">OF INSURANCE</div>
                <div class="headcenter">Russell Toal</div>
            </div>
            <div class="f-left w-45 headcenter"><img src="/images/osi-transparent.png" width="85px" height="60px" style="margin-bottom:15px">
                {{--        <div class="headcenterBold">SUPERINTENDENT OF INSURANCE</div>
                        <div class="headcenter">Russell Toal</div>--}}
            </div>
            <div class="f-right w-25 headcenter">
                <div class="headcenter">DEPUTY SUPERINTENDENT</div>
                <div class="headcenter">Robert E. Doucette, Jr.</div>
            </div>
        </div>
    </div>

    <div style="padding:10px">
        <div class="company-info f-left w-45 t-size12">
            <div>{{ $noticeDate }}</div>
            <br>
            <div>License #: {{ $entityData['naic_cocode'] }}</div>
            <div>{{ $entityData['company_name'] }}</div>
            <div>{{ $entityData['mlg_address1'] }}</div>
            <span>{{ $entityData['mailing_city'] }}</span>
            <span>{{ $entityData['mailing_state'] }}, </span>
            <span>{{ $entityData['mailing_zip'] }}</span>
        </div>

        <div class="company-info-2 w-100 t-left t-size10">
            <p>These are the <span style="color:red">ONLY</span> NPNs of the Affiliates assoicated to this renewal fee.</p>
        </div>

        <div style="padding:10px">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            @foreach($npnData->chunk(4) as $chunk)
                <tr>
                    @foreach($chunk as $npn)
                        <td style="text-align:center; border-right: 1px solid #777; border-left: 1px solid #777">{{ $npn->npn }}</td>
                    @endforeach
                </tr>
            @endforeach
        </table>
        </div>
@endsection
</html>
